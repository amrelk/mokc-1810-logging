# 2019 1810 Code

## Rio Ports
---
| PORT    | DIO     | CIRCUIT  |
| ------- |:-------:| --------:|
| 0 | BACK BALANCE SWITCH | ? |
| 1 | FRONT BALANCE SWITCH | ? |
| 2 | OPTICAL SENSOR | 40 |
| 3 | BACK LIMIT, UP  | 44 |
| 4 | BACK LIMIT, DOWN  | 43 |
| 5 | FRONT LIMIT, UP | 41 |
| 6 | FRONT LIMIT, DOWN  | 42 |
| 7 | TILT LIMIT | 45 |
| 8 | SCISSOR LIMIT, UP | 35 |

| PORT    | ANALOG     | CIRCUIT |
| ------- |:----------:| -------:|
| 0 | PRESSURE SENSOR | 0 | 30 |
| 1 | SCISSOR ENCODER | 1 | 31 |
| 2 |  |   |  |
| 3 |  |   |  |

| PORT        | PWM           | CIRCUIT |
| ----------- |:-------------:| -------:|
| 0 | LEFT DRIVE | 1-2 |
| 1 | RIGHT DRIVE | 3-4 |
| 2 |  |  |
| 3 | INTAKE MOTOR | 7 |
| 4 |  |  |
| 5 |  |  |
| 6 |  |  |
| 7 |  |  |
| 8 |  |  |
| 9 |  |  |

| PORT   | RELAY     | CIRCUIT |
| ------ |:---------:| -------:|
| 0 | SCISSOR LIFT | 5 |
| 1 | GEAR SHIFT RELAY | 6 |
| 2 |  |   |  |
| 3 |  |   |  |

## Pneumatics Control Module Ports

| PORT        | PCM           | CIRCUIT | COLOR |
| ----------- |:-------------:| -------:| -----:|
| 0 | EXTEND BACK   | 0 | Brown |
| 1 | EXTEND FRONT  | 1 | Blue |
| 2 | TILT SOLENOID | 2 | Orange |
| 3 | RETRACT BACK  | 3 | Green |
| 4 | INTAKE START  | 4 | Orange |
| 5 | INTAKE LIFT   | 5 | Green |
| 6 | SHIFTER       | 6 | Blue |
| 7 | RETRACT FRONT | 7 | Brown |

## Controls
| Controller    | FUNCTION|  BUTTON |
| ------------- |:-------:| -------:|
| JOYSTICK 0    | LEFT DRIVE | N/A |
| JOYSTICK 1    | RIGHT DRIVE | N/A |
| XBox Controller 1 | MANUAL RAISE SCISSOR |  RIGHT BUMPER |
| XBox Controller 1 | MANUAL LOWER SCISSOR  | LEFT BUMPER |
| XBox Controller 1 | ACTIVATE LIFT INTAKE SOLENOID | Y |
| XBox Controller 1 | DEACTIVATE LIFT INTAKE SOLENOID  | A |
| XBox Controller 1 | COMMAND GRAB HATCH  | B |
| XBox Controller 1 | COMMAND RELEASE HATCH  | X |
| XBox Controller 1 | RICKROLL | START |
| XBox Controller 1 | ABORT ENDGAME | BACK |
| XBox Controller 1 | COMMAND GEAR SHIFT UP | D-PAD 0,1,7 |
| XBox Controller 1 | COMMAND GEAR SHIFT UP | D-PAD 3,4,5 |
| XBox Controller 1 | GRAB BALL | Right Trigger |
| XBox Controller 1 | RELEASE BALL | Left Trigger |