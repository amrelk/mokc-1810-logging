package org.usd232.robotics.deepspace.log;

@FunctionalInterface
public interface UnsafeRunnable {
    void run() throws Throwable;
}
