package org.usd232.robotics.deepspace.log;

@FunctionalInterface
public interface UnsafeFunction<T> {
    T run() throws Throwable;
}
