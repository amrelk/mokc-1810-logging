package org.usd232.robotics.deepspace;

/**
 * Maps the ports and other values we need for the robot where we can change
 * stuff if wiring gets messed up.
 * 
 * @since 2017
 * 
 */
public interface RobotMap {
    // Controllers
    /**
     * The port for the left joystick.
     * 
     * @since 2019
     */
    public static final int LEFT_JOYSTICK_PORT = 0;
    /**
     * The port for the right joystick.
     * 
     * @since 2019
     */
    public static final int RIGHT_JOYSTICK_PORT = 1;
    /**
     * The port for the manipulator controller.
     * 
     * @since 2019
     */
    public static final int MANIPULATOR_PORT = 2;

    // PWMs
    /**
     * The port for the left motor.
     * 
     * @since 2017
     */
    public static final int LEFT_DRIVE_MOTOR_PORT = 0;
    /**
     * The type of the left motor.
     * 
     * @since 2017
     */
    public static final MotorID LEFT_DRIVE_MOTOR_TYPE = MotorID.Victor;
    /**
     * The port for the right motor.
     * 
     * @since 2017
     */
    public static final int RIGHT_DRIVE_MOTOR_PORT = 1;
    /**
     * The type of the right motor.
     * 
     * @since 2017
     */
    public static final MotorID RIGHT_DRIVE_MOTOR_TYPE = MotorID.Victor;
    /**
     * The port for the robot intake motor.
     * 
     * @since 2019
     */
    public static final int INTAKE_MOTOR_PORT = 3;
    /**
     * The type of motor for the robot intake.
     * 
     * @since 2019
     */
    public static final MotorID INTAKE_MOTOR_TYPE = MotorID.Spark;

    // DIOs
    /**
     * The port for the digital switch that sees if we are tilting towards the back
     * of the robot or not.
     * 
     * @since 2019
     */
    public static final int BACK_BALANCE_SWITCH_PORT = 0;
    /**
     * The port for the digital switch that sees if we are tilting towards the front
     * of the robot or not.
     * 
     * @since 2019
     */
    public static final int FRONT_BALANCE_SWITCH_PORT = 1;
    /**
     * The port for the optical sensor that we use to tell us if we are all the way
     * on the platform.
     * 
     * @since 2019
     */
    public static final int OPTICAL_SENSOR_PORT = 2;
    /**
     * The port for the magnetic switch that checks if the back pistons are pulled
     * up.
     * 
     * @since 2019
     */
    public static final int BACK_LIFT_UP_SWITCH_PORT = 3;
    /**
     * The port for the magnetic switch that checks if the back pistons are pushed
     * out.
     * 
     * @since 2019
     */
    public static final int BACK_LIFT_DOWN_SWITCH_PORT = 4;
    /**
     * The port for the magnetic switch that checks if the front pistons are pulled
     * up.
     * 
     * @since 2019
     */
    public static final int FRONT_LIFT_UP_SWITCH_PORT = 5;
    /**
     * The port for the magnetic switch that checks if the front pistons are pushed
     * out.
     * 
     * @since 2019
     */
    public static final int FRONT_LIFT_DOWN_SWITCH_PORT = 6;
    /**
     * The port for the magnetic switch thath checks if the tilt pistons are
     * retracted.
     * 
     * @since 2019
     */
    public static final int TILT_LIFT_SWITCH_PORT = 7;
    /**
     * The port for the limit switch to stop the scissor from going up more than is
     * possible so that they dont break it again.
     * 
     * @since 2019
     */
    public static final int SCISSOR_LIMIT_SWITCH_PORT = 8;

    // RELAYS
    /**
     * The port of the relay that controls the scissor lift winch.
     * 
     * @since 2019
     */
    public static final int SCISSOR_WINCH_RELAY_PORT = 0;
    /**
     * The port of the relay that controls the gear shifting of the robot.
     * 
     * @since 2019
     */
    public static final int GEAR_SHIFT_RELAY_PORT = 1;
    // PCM
    /**
     * The port for the valve that is used to extend the lift pistons on the back of
     * the robot.
     * 
     * @since 2019
     */
    public static final int EXTEND_BACK_LIFT_VALVE_PORT = 0;
    /**
     * The port for the valve that is used to extend the lift pistons on the front
     * of the robot.
     * 
     * @since 2019
     */
    public static final int EXTEND_FRONT_LIFT_VALVE_PORT = 1;
    /**
     * The port for the valve that controls the extension and retraction of the tilt
     * pistons.
     * 
     * @since 2019
     */
    public static final int TILT_LIFT_VALVE_PORT = 2;
    /**
     * The port for the valve that is used to retract the lift pistons on the back
     * of the robot.
     * 
     * @since 2019
     */
    public static final int RETRACT_BACK_VALVE_PORT = 3;
    /**
     * The port for the valve that is used to keep the intake always pushed out.
     * 
     * @since 2019
     */
    public static final int PUSH_INTAKE_VALVE_PORT = 4;
    /**
     * The port for the valve that is used to raise and lower the intake mechanism
     * for the balls.
     * 
     * @since 2019
     */
    public static final int RAISE_INTAKE_VALVE_PORT = 5;
    /**
     * This port is for the valve that controls the hatch grabber.
     * 
     * @since 2019
     */
    public static final int HATCH_GRABBER_PORT = 6;
    /**
     * The port for the valve that is used to retract the pistons on the front of
     * the robot.
     * 
     * @since 2019
     */
    public static final int RETRACT_FRONT_VALVE_PORT = 7;

    // Analog ports
    /**
     * The port for the analog pressure sensor that says how much pressure we have
     * in the tanks.
     * 
     * @since 2019
     */
    public static final int PRESSURE_SENSOR_PORT = 0;
}