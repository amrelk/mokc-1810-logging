package org.usd232.robotics.deepspace;

import org.usd232.robotics.deepspace.drive.GearDown;
import org.usd232.robotics.deepspace.drive.GearUp;
import org.usd232.robotics.deepspace.endgame.AbortEndgame;
import org.usd232.robotics.deepspace.endgame.RickRoll;
import org.usd232.robotics.deepspace.intake.ActivateValve;
import org.usd232.robotics.deepspace.intake.DeactivateValve;
import org.usd232.robotics.deepspace.intake.GrabBall;
import org.usd232.robotics.deepspace.intake.ReleaseBall;
import org.usd232.robotics.deepspace.lift.ManualLower;
import org.usd232.robotics.deepspace.lift.ManualRaise;
import org.usd232.robotics.deepspace.log.Logger;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj.buttons.Trigger;
import edu.wpi.first.wpilibj.command.Command;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 * 
 * @author Brian, Zach, Cody, Max
 * @since 2018
 */
public class OI extends Trigger implements RobotMap {
    /**
     * The Logger.
     * 
     * @since 2018
     */
    private static final Logger LOG = new Logger();

    public OI() {
        LOG.catchAll(() -> {
            ManipulatorXbox_RB.whileHeld(new ManualRaise());
            ManipulatorXbox_LB.whileHeld(new ManualLower());
            whileGreaterThan(Manipulator, 3, .1, new GrabBall());
            whileGreaterThan(Manipulator, 2, .1, new ReleaseBall());
            ManipulatorXbox_Y.whileHeld(new ActivateValve(IO.raiseIntakeValve));
            ManipulatorXbox_A.whileHeld(new DeactivateValve(IO.raiseIntakeValve));
            ManipulatorXbox_B.whenPressed(new ActivateValve(IO.hatchGrabberValve));
            ManipulatorXbox_X.whenPressed(new DeactivateValve(IO.hatchGrabberValve));
            ManipulatorXbox_Start.whenPressed(new RickRoll());
            ManipulatorXbox_Back.whenPressed(new AbortEndgame());
            
             whenPovIs(Manipulator, 0, new GearUp());
             whenPovIs(Manipulator, 1, new GearUp());
             whenPovIs(Manipulator, 3, new GearDown());
             whenPovIs(Manipulator, 4, new GearDown());
             whenPovIs(Manipulator, 5, new GearDown());
             whenPovIs(Manipulator, 7, new GearUp());
             

            // Manually testing endgame commands
            /*Manipulator2Xbox_Y.whenPressed(new BalanceLiftDigital());
            Manipulator2Xbox_B
                    .whenPressed(new DeactivateValve(IO.tiltLiftValve, CommandBase.climbingSubsystem::isTiltRetracted));
            Manipulator2Xbox_A.whenPressed(new DeactivateValve(IO.extendBackLiftValve, IO.retractBackLiftValve,
                    CommandBase.climbingSubsystem::isBackRetracted));
            Manipulator2Xbox_X.whenPressed(new DeactivateValve(IO.extendFrontLiftValve, IO.retractFrontLiftValve,
                    CommandBase.climbingSubsystem::isFrontRetracted));
            Manipulator2Xbox_RB.whenPressed(new DriveUntilSensorSays(IO.opticalSensor, -.45));*/
        });
    }

    // The controllers we are using this year
    public final Joystick Joystick0 = LOG.catchAll(() -> new Joystick(LEFT_JOYSTICK_PORT));
    public final Joystick Joystick1 = LOG.catchAll(() -> new Joystick(RIGHT_JOYSTICK_PORT));
    public final Joystick Manipulator = LOG.catchAll(() -> new Joystick(MANIPULATOR_PORT));
    public final Joystick Manipulator2 = LOG.catchAll(() -> new Joystick(MANIPULATOR_PORT + 1));
    public final Button Joystick0_Button1 = LOG.catchAll(() -> new JoystickButton(Joystick0, 1));
    public final Button Joystick0_Button2 = LOG.catchAll(() -> new JoystickButton(Joystick0, 2));
    public final Button Joystick0_Button3 = LOG.catchAll(() -> new JoystickButton(Joystick0, 3));
    public final Button Joystick0_Button4 = LOG.catchAll(() -> new JoystickButton(Joystick0, 4));
    public final Button Joystick0_Button5 = LOG.catchAll(() -> new JoystickButton(Joystick0, 5));
    public final Button Joystick0_Button6 = LOG.catchAll(() -> new JoystickButton(Joystick0, 6));
    public final Button Joystick0_Button7 = LOG.catchAll(() -> new JoystickButton(Joystick0, 7));
    public final Button Joystick0_Button8 = LOG.catchAll(() -> new JoystickButton(Joystick0, 8));
    public final Button Joystick0_Button9 = LOG.catchAll(() -> new JoystickButton(Joystick0, 9));
    public final Button Joystick0_Button10 = LOG.catchAll(() -> new JoystickButton(Joystick0, 10));
    public final Button Joystick0_Button11 = LOG.catchAll(() -> new JoystickButton(Joystick0, 11));
    public final Button Joystick1_Button1 = LOG.catchAll(() -> new JoystickButton(Joystick1, 1));
    public final Button Joystick1_Button2 = LOG.catchAll(() -> new JoystickButton(Joystick1, 2));
    public final Button Joystick1_Button3 = LOG.catchAll(() -> new JoystickButton(Joystick1, 3));
    public final Button Joystick1_Button4 = LOG.catchAll(() -> new JoystickButton(Joystick1, 4));
    public final Button Joystick1_Button5 = LOG.catchAll(() -> new JoystickButton(Joystick1, 5));
    public final Button Joystick1_Button6 = LOG.catchAll(() -> new JoystickButton(Joystick1, 6));
    public final Button Joystick1_Button7 = LOG.catchAll(() -> new JoystickButton(Joystick1, 7));
    public final Button Joystick1_Button8 = LOG.catchAll(() -> new JoystickButton(Joystick1, 8));
    public final Button Joystick1_Button9 = LOG.catchAll(() -> new JoystickButton(Joystick1, 9));
    public final Button Joystick1_Button10 = LOG.catchAll(() -> new JoystickButton(Joystick1, 10));
    public final Button Joystick1_Button11 = LOG.catchAll(() -> new JoystickButton(Joystick1, 11));

    // Xbox Controls
    public final Button ManipulatorXbox_A = LOG.catchAll(() -> new JoystickButton(Manipulator, 1));
    public final Button ManipulatorXbox_B = LOG.catchAll(() -> new JoystickButton(Manipulator, 2));
    public final Button ManipulatorXbox_X = LOG.catchAll(() -> new JoystickButton(Manipulator, 3));
    public final Button ManipulatorXbox_Y = LOG.catchAll(() -> new JoystickButton(Manipulator, 4));
    public final Button ManipulatorXbox_LB = LOG.catchAll(() -> new JoystickButton(Manipulator, 5));
    public final Button ManipulatorXbox_RB = LOG.catchAll(() -> new JoystickButton(Manipulator, 6));
    public final Button ManipulatorXbox_Back = LOG.catchAll(() -> new JoystickButton(Manipulator, 7));
    public final Button ManipulatorXbox_Start = LOG.catchAll(() -> new JoystickButton(Manipulator, 8));
    public final Button ManipulatorXbox_LStick = LOG.catchAll(() -> new JoystickButton(Manipulator, 9));
    public final Button ManipulatorXbox_RStick = LOG.catchAll(() -> new JoystickButton(Manipulator, 10));

    // Buttons to manually test each cylinder for endgame
    public final Button Manipulator2Xbox_A = LOG.catchAll(() -> new JoystickButton(Manipulator2, 1));
    public final Button Manipulator2Xbox_B = LOG.catchAll(() -> new JoystickButton(Manipulator2, 2));
    public final Button Manipulator2Xbox_X = LOG.catchAll(() -> new JoystickButton(Manipulator2, 3));
    public final Button Manipulator2Xbox_Y = LOG.catchAll(() -> new JoystickButton(Manipulator2, 4));
    public final Button Manipulator2Xbox_LB = LOG.catchAll(() -> new JoystickButton(Manipulator2, 5));
    public final Button Manipulator2Xbox_RB = LOG.catchAll(() -> new JoystickButton(Manipulator2, 6));
    public final Button Manipulator2Xbox_Back = LOG.catchAll(() -> new JoystickButton(Manipulator2, 7));
    public final Button Manipulator2Xbox_Start = LOG.catchAll(() -> new JoystickButton(Manipulator2, 8));
    public final Button Manipulator2Xbox_LStick = LOG.catchAll(() -> new JoystickButton(Manipulator2, 9));
    public final Button Manipulator2Xbox_RStick = LOG.catchAll(() -> new JoystickButton(Manipulator2, 10));

    abstract class Scheduler extends ButtonScheduler {
        @Override
        public void start() {
            super.start();
        }
    }

    @Override
    public boolean get() {
        return false;
    }

    /**
     * When an axis is less than a certain value run a certain command.
     * 
     * @param joystick What joystick the axis is on.
     * @param axis     What axis to look at.
     * @param value    What value should the axis be less than to run the command.
     * @param command  The command that should be ran when the axis is less than the
     *                 specified value.
     */
    public void whenLessThan(Joystick joystick, int axis, double value, Command command) {
        new Scheduler() {
            private boolean pressedLast = joystick.getRawAxis(axis) < value;

            @Override
            public void execute() {
                if (joystick.getRawAxis(axis) < value) {
                    if (!pressedLast) {
                        pressedLast = true;
                        command.start();
                    }
                } else {
                    pressedLast = false;
                }
            }
        }.start();
    }

    /**
     * When an axis is greater than a certain value run a certain command.
     * 
     * @param joystick What joystick the axis is on.
     * @param axis     What axis to look at.
     * @param value    What value should the axis be greater than to run the
     *                 command.
     * @param command  The command that should be ran when the axis is greater than
     *                 the specified value.
     */
    public void whenGreaterThan(Joystick joystick, int axis, double value, Command command) {
        new Scheduler() {
            private boolean pressedLast = joystick.getRawAxis(axis) > value;

            @Override
            public void execute() {
                if (joystick.getRawAxis(axis) > value) {
                    if (!pressedLast) {
                        pressedLast = true;
                        command.start();
                    }
                } else {
                    pressedLast = false;
                }
            }
        }.start();
    }

    /**
     * While an axis is greater than a certain value run a certain command.
     * 
     * @param joystick What joystick the axis is on.
     * @param axis     What axis to look at.
     * @param value    What value should the axis be greater than to run the
     *                 command.
     * @param command  The command that should be ran while the axis is greater than
     *                 the specified value.
     */
    public void whileGreaterThan(Joystick joystick, int axis, double value, Command command) {
        new Scheduler() {
            private boolean pressedLast = joystick.getRawAxis(axis) < value;

            @Override
            public void execute() {
                if (joystick.getRawAxis(axis) > value) {
                    if (!pressedLast) {
                        pressedLast = true;
                        command.start();
                    }
                } else {
                    if (pressedLast) {
                        command.cancel();
                        pressedLast = false;
                    }
                }
            }
        }.start();
    }

    /**
     * When a POV is at a certain value run a certain command.
     * 
     * @param joystick What joystick the POV is on.
     * @param value    What position the POV should be in (0-8), 0 being top, going
     *                 clockwise.
     * @param command  The command that should be ran while the POV is equal to the
     *                 specified value.
     */
    public void whenPovIs(Joystick joystick, int value, Command command) {
        new Scheduler() {
            @Override
            public void execute() {
                int currentValue = (int) (((joystick.getPOV() + 22.5) % 360) / 45);
                if (joystick.getPOV() == -1) {
                } else {
                    if (currentValue == value) {
                        command.start();
                    }
                }
            }
        }.start();
    }
}
