package org.usd232.robotics.deepspace.intake;

import org.usd232.robotics.deepspace.commands.CommandBase;

import org.usd232.robotics.deepspace.log.Logger;

public class GrabBall extends CommandBase {
    /**
     * The Logger.
     * 
     * @since 2018
     */
    private static final Logger LOG = new Logger();

    @Override
    protected void initialize() {
        LOG.catchAll(() -> {
            LOG.trace("Starting to grab a ball");
        });
    }

    @Override
    protected void execute() {
        LOG.catchAll(() -> {
            intakeSubsystem.runBallGrabber();
        });
    }

    @Override
    public boolean isFinished() {
        return LOG.catchAll(() -> {
            return false;
        }, true);
    }

    @Override
    protected void end() {
        LOG.catchAll(() -> {
            CommandBase.intakeSubsystem.stopBallGrabber();
            LOG.trace("Stopped grabbing a ball");
        });
    }

}