package org.usd232.robotics.deepspace.intake;

import java.util.function.Supplier;

import org.usd232.robotics.deepspace.commands.CommandBase;
import org.usd232.robotics.deepspace.log.Logger;

import edu.wpi.first.wpilibj.Solenoid;

/**
 * Deactivate is a flexible command with many different ways to run and use it
 * by deactivating a valve, deactivating a valve and ending based on a function,
 * deactivating and activating a valve, or deactivating a valve and activating a
 * valve and ending based off a function.
 *
 * @since 2019
 * @author Brian Parks
 */
public class DeactivateValve extends CommandBase {
    /**
     * The Logger.
     *
     * @since 2018
     */
    private static final Logger LOG = new Logger();
    /**
     * The valve that is getting deactivated.
     *
     * @since 2019
     */
    private final Solenoid dValve;
    /**
     * The valve that is getting activated (optional).
     *
     * @since 2019
     */
    private final Solenoid aValve;
    /**
     * The method that once it returns true, the command will end (optional).
     *
     * @since 2019
     */
    private final Supplier<Boolean> function;

    @Override
    protected void initialize() {
        LOG.catchAll(() -> {
            try {
                dValve.set(false);
                LOG.trace("Deactivating " + dValve.getName());
                aValve.set(true);
                LOG.trace("Activating " + aValve.getName());
            } catch (NullPointerException np) {
                // Dont want to see this
            } catch (Throwable t) {
                LOG.fatal(t);
            }
        });
    }

    @Override
    protected boolean isFinished() {
        try {
            return function.get();
        } catch (NullPointerException np) {
            // Dont want to see this spam
            return true;
        } catch (Throwable t) {
            LOG.fatal(t);
            return true;
        }
    }

    @Override
    public void end() {
        LOG.info("Finished DeactivateValve");
    }
    /**
     * Deactivates a specific valve and ends the command instantly.
     *
     * @param dValve The valve to deactivate.
     */
    public DeactivateValve(Solenoid dValve) {
        this(dValve, null, null);
    }

    /**
     * Deactivates a specific valve, and waits for the passed in method to return
     * true before the command ends.
     *
     * @param dValve   The valve to deactivate.
     * @param function The function that needs to return true for the command to
     *                 end.
     */
    public DeactivateValve(Solenoid dValve, Supplier<Boolean> function) {
        this(dValve, null, function);
    }

    /**
     * Deactivates a specific valve and activates another and ends the command
     * instantly.
     *
     * @param dValve The valve to deactivate.
     * @param aValve The valve to activate.
     */
    public DeactivateValve(Solenoid dValve, Solenoid aValve) {
        this(dValve, aValve, null);
    }

    /**
     * Deactivates a specific valve and activates another, then waits for the passed
     * in method to return true before the command ends.
     *
     * @param dValve   The valve to deactivate.
     * @param aValve   The valve to activate.
     * @param function The function that needs to return true.
     */
    public DeactivateValve(Solenoid dValve, Solenoid aValve, Supplier<Boolean> function) {
        this.function = function;
        this.dValve = dValve;
        this.aValve = aValve;
    }
}