package org.usd232.robotics.deepspace.intake;

import java.util.function.Supplier;

import org.usd232.robotics.deepspace.commands.CommandBase;
import org.usd232.robotics.deepspace.log.Logger;

import edu.wpi.first.wpilibj.Solenoid;

/**
 * ActivateValve is a flexible command with many different ways to run and use
 * it by activating a valve, activating a valve and ending based on a function,
 * activating and deactivating a valve, or activating a valve, deactivating a
 * valve, and ending based off a function.
 *
 * @since 2019
 * @author Brian Parks
 */
public class ActivateValve extends CommandBase {
    /**
     * The Logger.
     *
     * @since 2018
     */
    private static final Logger LOG = new Logger();
    /**
     * The valve that is getting activated.
     *
     * @since 2019
     */
    private final Solenoid aValve;
    /**
     * The valve that is getting deactivated (optional).
     *
     * @since 2019
     */
    private final Solenoid dValve;
    /**
     * The method that once it returns true, the command will end (optional).
     *
     * @since 2019
     */
    private final Supplier<Boolean> function;

    @Override
    protected void initialize() {
        LOG.catchAll(() -> {
            try {
                aValve.set(true);
                LOG.trace("Activating " + aValve.getName());
                dValve.set(false);
                LOG.trace("Deactivating " + dValve.getName());
            } catch (NullPointerException np) {
                // Do not want to see this
            } catch (Throwable t) {
                LOG.fatal(t);
            }
        });
    }

    @Override
    protected boolean isFinished() {
        try {
            return function.get();
        } catch (NullPointerException np) {
            // Dont want to see this spam
            return true;
        } catch (Throwable t) {
            LOG.fatal(t);
            return true;
        }
    }

    @Override
    public void end() {
        LOG.info("Finished ActivateValve");
    }
    
    /**
     * Activates a specific valve and ends the command instantly.
     *
     * @param aValve The valve to activate.
     */
    public ActivateValve(Solenoid aValve) {
        this( aValve, null, null );
    }

    /**
     * Activates a specific valve, and waits for the passed in method to return true
     * before the function ends.
     *
     * @param aValve The valve to activate.
     * @param function  The function that needs to return true.
     */
    public ActivateValve(Solenoid aValve, Supplier<Boolean> function) {
        this( aValve, null, function );
    }

    /**
     * Activates a specific valve and deactivates another and ends the command
     * instantly.
     *
     * @param aValve The valve to activate.
     * @param dValve The valve to deactivate.
     */
    public ActivateValve(Solenoid aValve, Solenoid dValve) {
        this( aValve, dValve, null );
    }

    /**
     * Activates a specific valve and deactivates another, then waits for the passed
     * in method to return true before the command ends.
     *
     * @param aValve The valve to activate.
     * @param dValve The valve to deactivate.
     * @param function  The function that needs to return true.
     */
    public ActivateValve(Solenoid aValve, Solenoid dValve, Supplier<Boolean> function) {
        this.function = function;
        this.aValve = aValve;
        this.dValve = dValve;
    }
}