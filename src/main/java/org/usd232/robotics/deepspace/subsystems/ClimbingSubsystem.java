package org.usd232.robotics.deepspace.subsystems;

import org.usd232.robotics.deepspace.IO;
import org.usd232.robotics.deepspace.log.Logger;

/**
 * The climbing subsystem, contains all of the methods for the climbing
 * subsystem.
 * 
 * @author Brian
 * @since 2019
 */
public class ClimbingSubsystem extends SubsystemBase {
    /**
     * The logger.
     * 
     * @since 2018
     */
    private static final Logger LOG = new Logger();

    /**
     * Checks if the front pistons of the climber are extended.
     * 
     * @return If the front pistons are extended.
     * @since 2019
     */
    public boolean isFrontExtended() {
        return !frontCylinderDownLimitSwitch.get();
    }

    /**
     * Checks if the front pistons of the climber are retracted.
     * 
     * @return If the front pistons are retracted.
     * @since 2019
     */
    public boolean isFrontRetracted() {
        return !frontCylinderUpLimitSwitch.get();
    }

    /**
     * Checks if the back pistons of the climber are extended.
     * 
     * @return If the back pistons are extended.
     * @since 2019
     */
    public boolean isBackExtended() {
        return !backCylinderDownLimitSwitch.get();
    }

    /**
     * Checks if the back pistons of the climber are retraced.
     * 
     * @return If the back pistons are retracted.
     * @since 2019
     */
    public boolean isBackRetracted() {
        return !backCylinderUpLimitSwitch.get();
    }

    /**
     * Checks if the tilt pistons of the climber are retracted.
     * 
     * @return If the tilt pistons are retracted.
     * @since 2019
     */
    public boolean isTiltRetracted() {
        return !tiltCylinderLimitSwitch.get();
    }

    /**
     * Checks if the robot is tilted backwards.
     * 
     * @return If the robot is tilted back.
     * @since 2019
     */
    public boolean isTiltedBack() {
        return !backBalanceSwitch.get();
    }

    /**
     * Checks if the robot is tilted forwards.
     * 
     * @return If the robot is tilted back.
     * @since 2019
     */
    public boolean isTiltedForward() {
        return !frontBalanceSwitch.get();
    }

    /**
     * Checks if the robot is balanced front to back.
     * 
     * @return If the robot is balanced front to back.
     * @since 2019
     */
    public boolean isStraight() {
        return !(!isTiltedBack() && !isTiltedForward());
    }

    /**
     * {@inheritDoc}
     */
    public void initDefaultCommand() {
        LOG.catchAll(() -> {
        });
    }

    /**
     * The constructor for the climbing subsystem, being used to name motors and
     * solenoids and such, just anything that needs to happen for the subsystem to
     * work.
     * 
     * @since 2019
     */
    public ClimbingSubsystem() {
        LOG.catchAll(() -> {
            IO.extendBackLiftValve.setName("Back lift extend pistons on port: " + EXTEND_BACK_LIFT_VALVE_PORT);
            IO.extendFrontLiftValve.setName("Front lift extend pistons on port: " + EXTEND_FRONT_LIFT_VALVE_PORT);
            IO.retractBackLiftValve.setName("Back lift retract pistons on port: " + RETRACT_BACK_VALVE_PORT);
            IO.retractFrontLiftValve.setName("Front lift retract pistons on port: " + RETRACT_FRONT_VALVE_PORT);
            IO.tiltLiftValve.setName("Tilt lift pistons on port: " + TILT_LIFT_VALVE_PORT);
            IO.backCylinderDownLimitSwitch.setName("Back down limit switch on port: " + BACK_LIFT_DOWN_SWITCH_PORT);
            IO.backCylinderUpLimitSwitch.setName("Back up limit switch on port: " + BACK_LIFT_UP_SWITCH_PORT);
            IO.frontCylinderDownLimitSwitch.setName("Front down limit switch on port: " + FRONT_LIFT_DOWN_SWITCH_PORT);
            IO.frontCylinderUpLimitSwitch.setName("Front up limit switch on port: " + FRONT_LIFT_UP_SWITCH_PORT);
            IO.tiltCylinderLimitSwitch.setName("Tilt limit switch on port: " + TILT_LIFT_SWITCH_PORT);
        });
    }

}