package org.usd232.robotics.deepspace.subsystems;

import org.usd232.robotics.deepspace.drive.DriveTank;
import org.usd232.robotics.deepspace.log.Logger;

import edu.wpi.first.wpilibj.Relay.Value;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;

/**
 * The drive subsystem. Contains all methods needed for drive commands
 * 
 * @author Brian, Zach D
 * @since 2017
 */
public class DriveSubsystem extends SubsystemBase {
    /**
     * The logger.
     * 
     * @since 2018
     */
    private static final Logger LOG = new Logger();

    /**
     * Class that controls both drive motors
     * 
     * @since 2018
     */
    private static DifferentialDrive robotDrive = LOG
            .catchAll(() -> new DifferentialDrive(leftDriveMotor, rightDriveMotor));

    /**
     * Drives the robot based on left and right speeds. Calls adjusted driving when
     * 1 or -1
     * 
     * @param left  speed
     * @param right speed
     * @since 2018
     */
    public void driveTank(double left, double right) {
        robotDrive.tankDrive(left, right);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initDefaultCommand() {
        LOG.catchAll(() -> {
            setDefaultCommand(new DriveTank());
        });
    }

    /**
     * The constructor for the driving subsystem, being used to name motors and
     * solenoids and such, just anything that needs to happen for the subsystem to
     * work.
     * 
     * @since 2017
     */
    public DriveSubsystem() {
        LOG.catchAll(() -> {
            shifterRelay.set(Value.kOn);
            opticalSensor.setName("Optical sensor on port: " + OPTICAL_SENSOR_PORT);
            gearDown();
        });
    
    }

    /**
     * The method that puts the robot into high gear.
     * 
     * @since 2019
     */
    public void gearUp() {
        shifterRelay.set(Value.kForward);
    }
    
    /**
     * The method that puts the robot into low gear.
     * 
     * @since 2019
     */
    public void gearDown() {
        shifterRelay.set(Value.kReverse);
    }
}