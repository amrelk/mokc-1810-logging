package org.usd232.robotics.deepspace.subsystems;

import edu.wpi.first.wpilibj.Relay.Value;
import org.usd232.robotics.deepspace.log.*;

/**
 * The subsystem for the Lift
 * 
 * @author Brian, Alex Whipple
 * @since 2018
 */
public class LiftSubsystem extends SubsystemBase {
    /**
     * The Logger.
     * 
     * @since 2018
     */
    private static final Logger LOG = new Logger();
    /**
     * Counter to count runs of lowerScissor method.
     * 
     * @since 2018
     */
    private int counter = 0;
    /**
     * The amount of runs the lowerScissor method should run for before stopping.
     * 
     * @since 2018
     */
    private int onTime = 5;
    /**
     * The amount of runs the lowerScissor method should be off for before
     * restarting.
     * 
     * @since 2018
     */
    private int offTime = 5;

    /**
     * Raises the scissor lift.
     * 
     * @since 2018
     */
    public void raiseScissor() {
        scissorRelay.set(Value.kOn);
        scissorRelay.set(Value.kForward);
    }

    /**
     * Lowers the scissor lift.
     * 
     * @since 2018
     */
    public void lowerScissor() {
        scissorRelay.set(Value.kOn);
        if (counter % (onTime + offTime) >= offTime) {
            scissorRelay.set(Value.kReverse);
        } else {
            stopScissor();
        }
        counter++;
    }

    /**
     * Stops the scissor lift.
     * 
     * @since 2018
     */
    public void stopScissor() {
        scissorRelay.set(Value.kOff);
    }

    /**
     * Checks if the scissor is at its max height based on the limit switch.
     * 
     * @since 2019
     */
    public boolean isScissorMaxed() {
        return !scissorUpLimitSwitch.get();
    }
    /**
     * Gets the value from the analog encoder for the height of the scissor.
     * 
     * @since 2019
     */
    public double getHeightValue() {
        return 0; // scissorValue.getVoltage();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void initDefaultCommand() {
    }

    /**
     * The constructor for the intake subsystem, being used to name motors and
     * solenoids and such, just anything that needs to happen for the subsystem to
     * work.
     * 
     * @since 2019
     */
    public LiftSubsystem() {
        LOG.catchAll(() -> {
            scissorRelay.setName("Scissor winch relay on port: " + SCISSOR_WINCH_RELAY_PORT);
        });
    }
}
