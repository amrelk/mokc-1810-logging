package org.usd232.robotics.deepspace.subsystems;

import org.usd232.robotics.deepspace.log.Logger;

/**
 * The subsystem for the intake mekanisms.
 * 
 * @author Brian
 * @since 2018
 */
public class IntakeSubsystem extends SubsystemBase {
    /**
     * The logger.
     * 
     * @since 2018
     */
    private static final Logger LOG = new Logger();

    /**
     * Enable the ball grabber belts.
     * 
     * @since 2019
     */
    public void runBallGrabber() {
        intakeMotor.set(1);
    }

    /**
     * Reverse the ball grabber belts.
     * 
     * @since 2019
     */
    public void reverseBallGrabber() {
        intakeMotor.set(-1);
    }

    /**
     * Stop the ball grabber belts.
     * 
     * @since 2019
     */
    public void stopBallGrabber() {
        intakeMotor.set(0);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void initDefaultCommand() {
    }

    /**
     * The constructor for the intake subsystem, being used to name motors and solenoids and such, just anything that
     * needs to happen for the subsystem to work.
     * 
     * @since 2019
     */
    public IntakeSubsystem() {
        LOG.catchAll(()-> {
            hatchGrabberValve.setName("Hatch grabber valve on port: " + HATCH_GRABBER_PORT);
            uprightIntakeValve.setName("Piston that pushes intake up on port: " + PUSH_INTAKE_VALVE_PORT);
            raiseIntakeValve.setName("Raise intake solenoid on port: " + RAISE_INTAKE_VALVE_PORT);
        });
    }
}
