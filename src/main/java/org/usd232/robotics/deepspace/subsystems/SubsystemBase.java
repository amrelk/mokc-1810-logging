package org.usd232.robotics.deepspace.subsystems;

import org.usd232.robotics.deepspace.IO;
import org.usd232.robotics.deepspace.RobotMap;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 * This is the base class for all of the subsystems.
 * 
 * @author Zach Deibert
 * @since 2017
 */
abstract class SubsystemBase extends Subsystem implements RobotMap, IO {
}