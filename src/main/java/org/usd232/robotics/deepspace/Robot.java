package org.usd232.robotics.deepspace;

import org.usd232.robotics.deepspace.commands.CommandBase;
import org.usd232.robotics.deepspace.intake.ActivateValve;
import org.usd232.robotics.deepspace.intake.DeactivateValve;
import org.usd232.robotics.deepspace.log.LogServer;
import org.usd232.robotics.deepspace.log.Logger;

import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.command.Scheduler;

/**
 * The first class that is called to set everything up
 * 
 * @author Brian, Zach, Cody
 * @since 2018
 */
public class Robot extends TimedRobot {
    /**
     * The Logger to use in the Robot class.
     * 
     * @since 2018
     */
    private static final Logger LOG = new Logger();

    /**
     * {@inheritDoc}
     */
    public void robotInit() {
        LOG.catchAll(() -> {
            CommandBase.init();
            Thread thread = new Thread(new LogServer());
            thread.start();
            CameraServer.getInstance().startAutomaticCapture();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void robotPeriodic() {
        LOG.catchAll(() -> {
            Scheduler.getInstance().run();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void disabledInit() {
        LOG.trace("Robot Disabled");
    }

    /**
     * {@inheritDoc}
     */
    public void disabledPeriodic() {
    }

    /**
     * {@inheritDoc}
     */
    public void autonomousInit() {
        LOG.catchAll(() -> {
            LOG.trace("Autonomous Initalized");
        });
    }

    /**
     * {@inheritDoc}
     */
    public void autonomousPeriodic() {
    }

    /**
     * {@inheritDoc}
     */
    public void teleopInit() {
        LOG.catchAll(() -> {
            new DeactivateValve(IO.extendBackLiftValve, IO.retractBackLiftValve,
                    CommandBase.climbingSubsystem::isBackRetracted).start();
            new DeactivateValve(IO.extendFrontLiftValve, IO.retractFrontLiftValve,
                    CommandBase.climbingSubsystem::isFrontRetracted).start();
            new DeactivateValve(IO.tiltLiftValve).start();
            new DeactivateValve(IO.raiseIntakeValve).start();
            new DeactivateValve(IO.hatchGrabberValve).start();
            new ActivateValve(IO.uprightIntakeValve).start();
            LOG.trace("Teleop Initalized");
        });
    }

    /**
     * {@inheritDoc}
     */
    public void teleopPeriodic() {
    }

    /**
     * {@inheritDoc}
     */
    public void testPeriodic() {
        LOG.catchAll(() -> {
        });
    }

    /**
     * {@inheritDoc}
     */
    public void testInit() {
        LOG.catchAll(() -> {
            LOG.trace("Test Initalized");
        });
    }
}
