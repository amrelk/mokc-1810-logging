package org.usd232.robotics.deepspace.endgame;

import org.usd232.robotics.deepspace.IO;
import org.usd232.robotics.deepspace.commands.CommandBase;
import org.usd232.robotics.deepspace.intake.ActivateValve;
import org.usd232.robotics.deepspace.intake.DeactivateValve;
import org.usd232.robotics.deepspace.log.Logger;

/**
 * BalancedLiftDigital raises the lift solenoids and balances based on 2 digital
 * sensors on the electrical panel.
 * 
 * @since 2019
 * @author Brian Parks
 */
public class BalanceLiftDigital extends CommandBase {
    /**
     * The Logger.
     * 
     * @since 2018
     */
    private static final Logger LOG = new Logger();
    /**
     * Tells us if the back of the robot is raising or not.
     * 
     * @since 2019
     */
    boolean raisingBack = false;
    /**
     * Tells us if the front of the robot is raising or not.
     * 
     * @since 2019
     */
    boolean raisingFront = false;

    @Override
    protected void initialize() {
        LOG.info("Activating " + IO.extendFrontLiftValve.getName() + " and " + IO.extendBackLiftValve.getName()
                + " for a balance.");
        new ActivateValve(IO.extendFrontLiftValve, IO.retractFrontLiftValve).start();
        new ActivateValve(IO.extendBackLiftValve, IO.retractBackLiftValve).start();
        raisingBack = true;
        raisingFront = true;
    }

    @Override
    protected void execute() {
        if (climbingSubsystem.isTiltedForward() == true) {
            new DeactivateValve(IO.extendFrontLiftValve).start();
            raisingFront = false;
            LOG.info("Deactivating " + IO.extendFrontLiftValve.getName());
        }

        if (climbingSubsystem.isTiltedBack() == true) {
            new DeactivateValve(IO.extendBackLiftValve).start();
            raisingBack = false;
            LOG.info("Deactivating " + IO.extendBackLiftValve.getName());
        }

        if (climbingSubsystem.isStraight()) {
            if (!raisingFront) {
                new ActivateValve(IO.extendFrontLiftValve).start();
                raisingFront = true;
                LOG.info("Activating " + IO.extendFrontLiftValve.getName());
            }
            if (!raisingBack) {
                new ActivateValve(IO.extendBackLiftValve).start();
                raisingBack = true;
                LOG.info("Activating " + IO.extendBackLiftValve.getName());
            }
        }
    }

    @Override
    protected boolean isFinished() {
        LOG.debug("Front is: " + climbingSubsystem.isFrontExtended() + " Back is: " + climbingSubsystem.isBackExtended());
        if (climbingSubsystem.isBackExtended()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected void end() {
        LOG.trace("Finished lifting " + IO.extendFrontLiftValve.getName() + " and " + IO.extendBackLiftValve.getName()
                + " with a balance.");

    }
}