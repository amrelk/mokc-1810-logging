package org.usd232.robotics.deepspace.endgame;

import org.usd232.robotics.deepspace.IO;
import org.usd232.robotics.deepspace.commands.CommandBase;
import org.usd232.robotics.deepspace.log.Logger;
import org.usd232.robotics.deepspace.intake.*;
import edu.wpi.first.wpilibj.command.Scheduler;

/**
 * AbortEndgame takes and resets the climbing solenoids to their default
 * positions.
 * 
 * @since 2019
 * @author Brian Parks
 */
public class AbortEndgame extends CommandBase {
    /**
     * The Logger.
     * 
     * @since 2018
     */
    private static final Logger LOG = new Logger();

    @Override
    protected void initialize() {
        LOG.catchAll(() -> {
            Scheduler.getInstance().removeAll();
            new DeactivateValve(IO.tiltLiftValve).start();
            new DeactivateValve(IO.extendFrontLiftValve, IO.retractFrontLiftValve).start();
            new DeactivateValve(IO.extendBackLiftValve, IO.retractBackLiftValve).start();
            LOG.trace("Aborting endgame!");
        });
    }

    @Override
    protected boolean isFinished() {
        return true;
    }
}