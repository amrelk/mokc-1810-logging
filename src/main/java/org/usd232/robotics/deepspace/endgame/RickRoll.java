package org.usd232.robotics.deepspace.endgame;

import org.usd232.robotics.deepspace.IO;
import org.usd232.robotics.deepspace.commands.CommandBase;
import org.usd232.robotics.deepspace.drive.Delay;
import org.usd232.robotics.deepspace.drive.DriveForTime;
import org.usd232.robotics.deepspace.drive.DriveUntilSensorSays;
import org.usd232.robotics.deepspace.intake.ActivateValve;
import org.usd232.robotics.deepspace.intake.DeactivateValve;
import org.usd232.robotics.deepspace.log.Logger;

/**
 * RickRoll is the entire endgame sequence that will run automatically on the
 * press of a button.
 * 
 * @since 2019
 * @author Brian Parks
 */
public class RickRoll extends CommandBase {
    /**
     * The Logger.
     * 
     * @since 2018
     */
    private static final Logger LOG = new Logger();

    // Lifting
    /**
     * This is the command to lift the robot and use the 2 switches to fix the
     * tilting of it so that it does not go one way or another.
     * 
     * @since 2019
     */
    private BalanceLiftDigital balanceLiftDigital;
    /**
     * This boolean shows whether or not the raise function has finished and the
     * next command has been started.
     * 
     * @since 2019
     */
    private boolean hasRaised = false;

    // First delay.
    /**
     * This is the delay between the initial raising of the robot and the tilting of
     * robot.
     * 
     * @since 2019
     */
    private Delay delayOne;
    /**
     * This boolean shows whether or not the first delay has finished and the next
     * command has been started.
     * 
     * @since 2019
     */
    private boolean hasDelayedOnce = false;

    // Retracting tilt solenoids.
    /**
     * This command disables the tilt solenoids to tilt the robot.
     * 
     * @since 2019
     */
    private ActivateValve retractTilt;
    /**
     * This boolean shows whether or not the robot has tilted and the next command
     * has been started.
     * 
     * @since 2019
     */
    private boolean hasRetractedTilt = false;

    // Second delay.
    /**
     * This is the delay between the tilting of robot and the raising of the back
     * cylinders.
     * 
     * @since 2019
     */
    private Delay delayTwo;
    /**
     * This boolean shows whether or not the second delay has finished and the next
     * command has been started.
     * 
     * @since 2019
     */
    private boolean hasDelayedTwice = false;

    // Retract back cylinders.
    /**
     * This command disables the back solenoids of the robot.
     * 
     * @since 2019
     */
    private DeactivateValve retractBack;
    /**
     * This boolean shows whether or not the the back cylinders have been retracted
     * and the next command has been started.
     * 
     * @since 2019
     */
    private boolean hasRetractedBack = false;

    // Drive forward.
    /**
     * This command drives the robot until the sensor on the back of the robot trips
     * and tells us we have gone far enough.
     * 
     * @since 2019
     */
    private DriveUntilSensorSays drive;
    /**
     * This boolean shows whether or not the robot has drove and the next command
     * has been started.
     * 
     * @since 2019
     */
    private boolean hasDrove = false;

    // Retracting front cylinders.
    /**
     * This command retracts the front cylinders of the robot.
     * 
     * @since 2019
     */
    private DeactivateValve retractFront;
    private boolean hasRetractedFront = false;
    //Second drive.
    private DriveForTime driveTime;

    @Override
    protected void initialize() {
        LOG.catchAll(() -> {
            LOG.trace("Starting endgame!!");
            balanceLiftDigital = new BalanceLiftDigital();
            balanceLiftDigital.start();
        });
    }

    @Override
    protected void execute() {
        try {
            if (balanceLiftDigital.isCompleted() && !hasRaised) {
                hasRaised = true;
                delayOne = new Delay(500);
                delayOne.start();
            } else if (delayOne.isCompleted() && !hasDelayedOnce) {
                hasDelayedOnce = true;
                retractTilt = new ActivateValve(IO.tiltLiftValve, climbingSubsystem::isTiltRetracted);
                retractTilt.start();
            } else if (retractTilt.isCompleted() && !hasRetractedTilt) {
                hasRetractedTilt = true;
                delayTwo = new Delay(500);
                delayTwo.start();
            } else if (delayTwo.isCompleted() && !hasDelayedTwice) {
                hasDelayedTwice = true;
                retractBack = new DeactivateValve(IO.extendBackLiftValve, IO.retractBackLiftValve,
                        climbingSubsystem::isBackRetracted);
                retractBack.start();
            } else if (retractBack.isCompleted() && !hasRetractedBack) {
                hasRetractedBack = true;
                drive = new DriveUntilSensorSays(IO.opticalSensor, -.45);
                drive.start();
            } else if (drive.isCompleted() && !hasDrove) {
                hasDrove = true;
                retractFront = new DeactivateValve(IO.extendFrontLiftValve, IO.retractFrontLiftValve,
                        climbingSubsystem::isFrontRetracted);
                retractFront.start();
            } else if (retractFront.isCompleted() && !hasRetractedFront) {
                hasRetractedFront = true;
                driveTime = new DriveForTime(1000, -.4);
                driveTime.start();
            }
        } catch (NullPointerException n) {
            // We dont want to see this since a lot of null pointer exceptions are thrown
            // when certain commands have not been started.
        } catch (Throwable t) {
            LOG.fatal(t);
        }
    }

    @Override
    protected boolean isFinished() {
        try {
            return driveTime.isCompleted();
        } catch (NullPointerException np) {
            return false;
        } catch (Throwable t) {
            LOG.fatal(t);
            return true;
        }
    }

    @Override
    protected void end() {
        try {
            balanceLiftDigital = null;
            hasRaised = false;
            delayOne = null;
            hasDelayedOnce = false;
            retractTilt = null;
            hasRetractedTilt = false;
            delayTwo = null;
            hasDelayedTwice = false;
            retractBack = null;
            hasRetractedBack = false;
            drive = null;
            hasDrove = false;
            retractFront = null;
            hasRetractedFront = false;
            driveTime = null;
        } catch (NullPointerException np) {
            // Dont want to see this
        } catch (Throwable t) {
            LOG.fatal(t);
        }
        LOG.trace("Never gonna give you up.");
        LOG.trace("Never gonna let you down.");
        LOG.trace("Never gonna run around, and, desert you");
        LOG.trace("Never gonna make you cry");
        LOG.trace("Never gonna say goodbye");
        LOG.trace("Never gonna tell a lie, and hurt you");
    }

    /**
     * @{inheritDoc}
     */
    @Override
    protected void interrupted() {
        try {
            if (!hasRaised) {
                delayOne.cancel();
            } else if (!hasDelayedOnce) {
                retractTilt.cancel();
            } else if (!hasDelayedTwice) {
                delayTwo.cancel();
            } else if (!hasRetractedTilt) {
                retractBack.cancel();
            } else if (!hasRetractedBack) {
                drive.cancel();
            } else if (!hasDrove) {
                retractFront.cancel();
            }
        } catch (NullPointerException np) {
            // We dont want to see this since a lot of null pointer exceptions are thrown
            // when certain commands have not been started.
        } catch (Throwable t) {
            LOG.fatal(t);
        }
        end();
    }
}