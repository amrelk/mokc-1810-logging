package org.usd232.robotics.deepspace.lift;

import org.usd232.robotics.deepspace.commands.CommandBase;
import org.usd232.robotics.deepspace.log.Logger;

/**
 * The command to raise the lift.
 * 
 * @author Brian, Matthew, Alex
 * 
 * @since 2018
 */
public class RaiseToLevel extends CommandBase {
    /**
     * The Logger.
     * 
     * @since 2018
     */
    private static final Logger LOG = new Logger();
    /**
     * Value that we are raising to.
     * 
     * @since 2018
     */
    private double raiseValue = 0;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void initialize() {
        LOG.catchAll(() -> {
            LOG.info("Raising Lift To " + raiseValue);
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void execute() {
        LOG.catchAll(() -> {
            liftSubsystem.raiseScissor();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isFinished() {
        return LOG.catchAll(() -> {
            if (liftSubsystem.getHeightValue() >= raiseValue) {
                LOG.info("Stop For Encoder");
                return true;
            } else {
                return false;
            }
        }, true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void end() {
        LOG.catchAll(() -> {
            liftSubsystem.stopScissor();
        });
    }

    /**
     * Raises the lift of the robot to specified potentiometer value.
     * 
     * @param raiseValue the value the robot the lift to
     * @since 2018
     */
    public RaiseToLevel(double raiseValue) {
        this.raiseValue = raiseValue;
    }
}
