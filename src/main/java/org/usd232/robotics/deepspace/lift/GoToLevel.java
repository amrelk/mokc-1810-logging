package org.usd232.robotics.deepspace.lift;

import java.util.function.Supplier;
import org.usd232.robotics.deepspace.commands.CommandBase;
import org.usd232.robotics.deepspace.log.Logger;

/**
 * The command to take the lift to a specific step.
 * 
 * @author Brian, Alex
 * @since 2018
 */
public class GoToLevel extends CommandBase {
    /**
     * The target potentiometer value to go to on the robot.
     * 
     * @since 2018
     */
    private Supplier<Double> targetEncoderValue;
    /**
     * The command that the robot runs based on where the robot is.
     * 
     * @since 2018
     */
    private CommandBase command;
    /**
     * The Logger.
     * 
     * @since 2018
     */
    private static final Logger LOG = new Logger();

    /**
     * {@inheritDoc}
     */
    @Override
    protected void initialize() {
        LOG.catchAll(() -> {
            double currentEncoderValue = liftSubsystem.getHeightValue();
            LOG.info("Current Value Of Encoder " + currentEncoderValue);
            if (targetEncoderValue.get() >= currentEncoderValue) {
                LOG.info("Raising to the height of " + this.targetEncoderValue.get());
                command = new RaiseToLevel(targetEncoderValue.get());
            } else if (targetEncoderValue.get() <= currentEncoderValue) {
                LOG.info("Lowering to the height of " + this.targetEncoderValue.get());
                command = new LowerToLevel(targetEncoderValue.get());
            }
            command.initializePublic();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void execute() {
        LOG.catchAll(() -> {
            command.executePublic();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isFinished() {
        return LOG.catchAll(() -> {
            if (command == null) {
                return false;
            } else {
                return command.isFinishedPublic();
            }
        }, true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void end() {
        LOG.catchAll(() -> {
            command.endPublic();
            command = null;
        });
    }

    /**
     * Takes the lift of the robot to specified encoder value.
     * 
     * @param targetValue The level we are targeting.
     * @since 2018
     */
    public GoToLevel(Supplier<Double> targetValue) {
        this.targetEncoderValue = targetValue;
    }
}
