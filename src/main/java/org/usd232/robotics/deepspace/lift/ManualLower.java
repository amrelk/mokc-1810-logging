package org.usd232.robotics.deepspace.lift;

import org.usd232.robotics.deepspace.commands.CommandBase;
import org.usd232.robotics.deepspace.log.Logger;

/**
 * The command to lower the lift manually.
 * 
 * @author Brian, Alex Whipple
 * @since 2018
 */
public class ManualLower extends CommandBase {
    /**
     * The Logger.
     * 
     * @since 2018
     */
    private static final Logger LOG = new Logger();

    /**
     * {@inheritDoc}
     */
    @Override
    protected void initialize() {
        LOG.catchAll(() -> {
            LOG.info("Manually lowering lift");
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void execute() {
        LOG.catchAll(() -> {
            liftSubsystem.lowerScissor();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isFinished() {
        return LOG.catchAll(() -> {
            return false;
        }, true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void end() {
        LOG.catchAll(() -> {
            LOG.trace("Stopped lowering the lift");
            liftSubsystem.stopScissor();
        });
    }
}
