package org.usd232.robotics.deepspace.lift;

import org.usd232.robotics.deepspace.commands.CommandBase;
import org.usd232.robotics.deepspace.log.Logger;

/**
 * The command to lower the lift.
 * 
 * @author Brian, Matthew, Alex
 * @since 2018
 */
public class LowerToLevel extends CommandBase {
    /**
     * The Logger.
     * 
     * @since 2018
     */
    private static final Logger LOG = new Logger();
    /**
     * Value that we are lowering to.
     * 
     * @since 2018
     */
    private double lowerValue = 0;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void initialize() {
        LOG.catchAll(() -> {
            LOG.info("Lowering Lift To " + lowerValue);
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void execute() {
        LOG.catchAll(() -> {
            liftSubsystem.raiseScissor();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isFinished() {
        return LOG.catchAll(() -> {
            if (liftSubsystem.getHeightValue() <= lowerValue) {
                LOG.info("Stop For Encoder");
                return true;
            } else {
                return false;
            }
        }, true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void end() {
        LOG.catchAll(() -> {
            liftSubsystem.stopScissor();
        });
    }

    /**
     * Lowers the lift of the robot to specified potentiometer value.
     * 
     * @param lowerValue the value the robot the lift to
     * @since 2018
     */
    public LowerToLevel(double lowerValue) {
        this.lowerValue = lowerValue;
    }
}
