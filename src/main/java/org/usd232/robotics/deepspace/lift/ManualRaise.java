package org.usd232.robotics.deepspace.lift;

import org.usd232.robotics.deepspace.commands.CommandBase;
import org.usd232.robotics.deepspace.log.Logger;

/**
 * The command to raise the lift manually.
 * 
 * @author Brian, Matthew
 * @since 2018
 */
public class ManualRaise extends CommandBase {
    /**
     * The Logger.
     * 
     * @since 2018
     */
    private static final Logger LOG = new Logger();

    /**
     * {@inheritDoc}
     */
    @Override
    protected void initialize() {
        LOG.catchAll(() -> {
            LOG.info("Manually Raising Lift");
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void execute() {
        LOG.catchAll(() -> {
            liftSubsystem.raiseScissor();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isFinished() {
        return LOG.catchAll(() -> {
            return liftSubsystem.isScissorMaxed();
        }, true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void end() {
        LOG.catchAll(() -> {
            LOG.trace("Stopped raising the lift");
            liftSubsystem.stopScissor();
        });
    }
}
