package org.usd232.robotics.deepspace;

import org.usd232.robotics.deepspace.log.Logger;

import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Relay;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.SpeedController;

/**
 * This class contains all I/O initialization. This class is the ONLY class
 * allowed to initialize I/O devices (motors, encoders, sensors, etc.).
 * 
 * @author Zach Deibert, Brian Parks, Alex Whipple
 * @since 2018
 * 
 */
public interface IO extends RobotMap {
        /**
         * The logger.
         * 
         * @since 2018
         */
        public static final Logger DO_NOT_USE_THIS_LOGGER = new Logger();

        // PWMs
        /**
         * The left drive motor.
         * 
         * @since 2018
         */
        public static final SpeedController leftDriveMotor = DO_NOT_USE_THIS_LOGGER
                        .catchAll(() -> IOFactory.motor(LEFT_DRIVE_MOTOR_PORT, LEFT_DRIVE_MOTOR_TYPE));
        /**
         * The right drive motor.
         * 
         * @since 2018
         */
        public static final SpeedController rightDriveMotor = DO_NOT_USE_THIS_LOGGER
                        .catchAll(() -> IOFactory.motor(RIGHT_DRIVE_MOTOR_PORT, RIGHT_DRIVE_MOTOR_TYPE));
        /**
         * The motor for the intake.
         * 
         * @since 2019
         */
        public static final SpeedController intakeMotor = DO_NOT_USE_THIS_LOGGER
                        .catchAll(() -> IOFactory.motor(INTAKE_MOTOR_PORT, INTAKE_MOTOR_TYPE));

        // DIOs
        /**
         * The switch that checks if the robot is tilting backwards or not.
         * 
         * @since 2019
         */
        public static final DigitalInput backBalanceSwitch = DO_NOT_USE_THIS_LOGGER
                        .catchAll(() -> new DigitalInput(BACK_BALANCE_SWITCH_PORT));
        /**
         * The switch that checks if the robot is tilting forwards or not.
         * 
         * @since 2019
         */
        public static final DigitalInput frontBalanceSwitch = DO_NOT_USE_THIS_LOGGER
                        .catchAll(() -> new DigitalInput(FRONT_BALANCE_SWITCH_PORT));
        /**
         * The optical sensor that tells us whether or not we have drove forward enough
         * to be able to raise the last pistons of the climber.
         * 
         * @since 2019
         */
        public static final DigitalInput opticalSensor = DO_NOT_USE_THIS_LOGGER
                        .catchAll(() -> new DigitalInput(OPTICAL_SENSOR_PORT));
        /**
         * The reed sensor on the front cylinders of the robot that see if the pistons
         * is retracted.
         * 
         * @since 2019
         */
        public static final DigitalInput frontCylinderUpLimitSwitch = DO_NOT_USE_THIS_LOGGER
                        .catchAll(() -> new DigitalInput(FRONT_LIFT_UP_SWITCH_PORT));
        /**
         * The reed sensor on the back cylinders of the robot that see if the pistons is
         * retracted.
         * 
         * @since 2019
         */
        public static final DigitalInput backCylinderUpLimitSwitch = DO_NOT_USE_THIS_LOGGER
                        .catchAll(() -> new DigitalInput(BACK_LIFT_UP_SWITCH_PORT));
        /**
         * The reed sensor on the front cylinders of the robot that see if the pistons
         * is extended.
         * 
         * @since 2019
         */
        public static final DigitalInput frontCylinderDownLimitSwitch = DO_NOT_USE_THIS_LOGGER
                        .catchAll(() -> new DigitalInput(FRONT_LIFT_DOWN_SWITCH_PORT));
        /**
         * The reed sensor on the back cylinders of the robot that see if the pistons is
         * extended.
         *
         * @since 2019
         */
        public static final DigitalInput backCylinderDownLimitSwitch = DO_NOT_USE_THIS_LOGGER
                        .catchAll(() -> new DigitalInput(BACK_LIFT_DOWN_SWITCH_PORT));
        /**
         * The reed sensor that sees if the pistons used to tilt the robot is retracted.
         * 
         * @since 2019
         */
        public static final DigitalInput tiltCylinderLimitSwitch = DO_NOT_USE_THIS_LOGGER
                        .catchAll(() -> new DigitalInput(TILT_LIFT_SWITCH_PORT));
        /**
         * The limit switch that sees if the lift is all the way up.
         * 
         * @since 2019
         */
        public static final DigitalInput scissorUpLimitSwitch = DO_NOT_USE_THIS_LOGGER
                        .catchAll(() -> new DigitalInput(SCISSOR_LIMIT_SWITCH_PORT));

        // Relays
        /**
         * The relay that controlls the scissor lift of the robot.
         * 
         * @since 2019
         */
        public static final Relay scissorRelay = DO_NOT_USE_THIS_LOGGER
                        .catchAll(() -> new Relay(SCISSOR_WINCH_RELAY_PORT));
        /**
         * The relay that controlls the gear shifting of the robot.
         * 
         * @since 2019
         */
        public static final Relay shifterRelay = DO_NOT_USE_THIS_LOGGER
                        .catchAll(() -> new Relay(GEAR_SHIFT_RELAY_PORT));


        // PCMs
        /**
         * The piston that pushes the intake up into its default position.
         * 
         * @since 2019
         */
        public static final Solenoid uprightIntakeValve = DO_NOT_USE_THIS_LOGGER
                        .catchAll(() -> new Solenoid(PUSH_INTAKE_VALVE_PORT));
        /**
         * The piston that lifts the intake up and down to grab balls.
         * 
         * @since 2019
         */
        public static final Solenoid raiseIntakeValve = DO_NOT_USE_THIS_LOGGER
                        .catchAll(() -> new Solenoid(RAISE_INTAKE_VALVE_PORT));
         /**
         * The soleniod that shifts the robot into low of high gear.
         * 
         * @since 2019
         */
        public static final Solenoid hatchGrabberValve = DO_NOT_USE_THIS_LOGGER
                        .catchAll(() -> new Solenoid(HATCH_GRABBER_PORT));                
        /**
         * The valve that when enabled extends the front pistons of the climber.
         * 
         * @since 2019
         */
        public static final Solenoid extendFrontLiftValve = DO_NOT_USE_THIS_LOGGER
                        .catchAll(() -> new Solenoid(EXTEND_FRONT_LIFT_VALVE_PORT));
        /**
         * The valve that when enabled extends the back pistons of the climber.
         * 
         * @since 2019
         */
        public static final Solenoid extendBackLiftValve = DO_NOT_USE_THIS_LOGGER
                        .catchAll(() -> new Solenoid(EXTEND_BACK_LIFT_VALVE_PORT));
        /**
         * The valve that when enabled retracts the front pistons of the climber.
         * 
         * @since 2019
         */
        public static final Solenoid retractFrontLiftValve = DO_NOT_USE_THIS_LOGGER
                        .catchAll(() -> new Solenoid(RETRACT_FRONT_VALVE_PORT));
        /**
         * The valve that when enabled retracts the back pistons of the climber.
         * 
         * @since 2019
         */
        public static final Solenoid retractBackLiftValve = DO_NOT_USE_THIS_LOGGER
                        .catchAll(() -> new Solenoid(RETRACT_BACK_VALVE_PORT));
        /**
         * The valve that when enabled tilts the back climbing pistons.
         * 
         * @since 2019
         */
        public static final Solenoid tiltLiftValve = DO_NOT_USE_THIS_LOGGER
                        .catchAll(() -> new Solenoid(TILT_LIFT_VALVE_PORT));

        // Analog Sensors
        /**
         * The sensor that guages the pressure in the air tanks.
         * 
         * @since 2019
         */
        public static final AnalogInput pressureSensor = DO_NOT_USE_THIS_LOGGER
                        .catchAll(() -> new AnalogInput(PRESSURE_SENSOR_PORT));

}