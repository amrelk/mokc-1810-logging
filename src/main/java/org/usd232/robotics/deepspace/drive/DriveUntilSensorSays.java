package org.usd232.robotics.deepspace.drive;

import org.usd232.robotics.deepspace.commands.CommandBase;
import org.usd232.robotics.deepspace.log.Logger;

import edu.wpi.first.wpilibj.DigitalInput;

public class DriveUntilSensorSays extends CommandBase {
    /**
     * The Logger.
     * 
     * @since 2018
     */
    private static final Logger LOG = new Logger();
    /**
     * The sensor that the robot is checking.
     * 
     * @since 2019
     */
    private DigitalInput sensor;
    /**
     * The speed the robot is moving.
     * 
     * @since 2019
     */
    private double speed;

    @Override
    protected void execute() {
        LOG.catchAll(() -> {
            CommandBase.driveSubsystem.driveTank(speed, speed);
        });
    }

    @Override
    protected boolean isFinished() {
        return !sensor.get();
    }

    @Override
    protected void end() {
        LOG.info("Ending drive");
        CommandBase.driveSubsystem.driveTank(0, 0);
    }

    /**
     * Drives until the sensor hits the passed in value.
     * 
     * @param sensor The sensor that we are checking.
     * @param speed  The speed in which the robot should go.
     * @since 2019
     * @author Daanish Suhail, Brian Parks
     */
    public DriveUntilSensorSays(DigitalInput sensor, double speed) {
        if (speed < 0) {
            LOG.trace("Starting Drive Backward");
        } else {
            LOG.trace("Starting Drive Forward");
        }
        requires(driveSubsystem);
        this.sensor = sensor;
        this.speed = speed;
    }
}