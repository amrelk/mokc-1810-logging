package org.usd232.robotics.deepspace.drive;

import org.usd232.robotics.deepspace.commands.CommandBase;
import org.usd232.robotics.deepspace.log.Logger;

/**
 * Puts the robot into high gear using a relay controller.
 * 
 * @author Brian
 * @since 2019
 */
public class GearUp extends CommandBase {
    /**
     * The logger.
     * 
     * @since 2018
     */
    private static final Logger LOG = new Logger();

    /**
     * {@inheritDoc}
     */
    @Override
    protected void initialize() {
        LOG.catchAll(() -> {
            LOG.trace("Running GearUp initialize");
            driveSubsystem.gearUp();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isFinished() {
        return LOG.catchAll(() -> {
            return true;
        }, true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void end() {
        LOG.catchAll(() -> {
        });
    }
}
