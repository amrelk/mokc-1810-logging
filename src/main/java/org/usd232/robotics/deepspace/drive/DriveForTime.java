package org.usd232.robotics.deepspace.drive;

import org.usd232.robotics.deepspace.commands.CommandBase;
import org.usd232.robotics.deepspace.log.Logger;

public class DriveForTime extends CommandBase {
    /**
     * The Logger.
     * 
     * @since 2018
     */
    private static final Logger LOG = new Logger();
    /**
     * The amount if time in milliseconds.
     * 
     * @since 2019
     */
    private long millis;
    /**
     * The start time of the command.
     * 
     * @since 2019
     */
    private long startTime;
    /**
     * The speed the robot is moving.
     * 
     * @since 2019
     */
    private double speed;

    @Override
    protected void initialize() {
        this.startTime = System.currentTimeMillis();
        LOG.debug(Long.toString(startTime));
    }

    @Override
    protected void execute() {
        LOG.catchAll(() -> {
            LOG.debug("Executing");
            CommandBase.driveSubsystem.driveTank(speed, speed);
        });
    }

    @Override
    protected boolean isFinished() {
        return LOG.catchAll(() -> {
            LOG.debug("Currently at " + System.currentTimeMillis() + " needs to be "
                    + Long.toString((startTime + millis)));
            return System.currentTimeMillis() >= startTime + millis;
        }, true);
    }

    @Override
    protected void end() {
        LOG.info("Ending drive");
        CommandBase.driveSubsystem.driveTank(0, 0);
    }

    /**
     * Drives until the amount of time passed in passes.
     * 
     * @param millis The amount of time in milliseconds for the robot to drive.
     * @param speed  The speed in which the robot should go.
     * @since 2019
     * @author Daanish Suhail, Brian Parks
     */
    public DriveForTime(long millis, double speed) {
        requires(driveSubsystem);
        if (speed < 0) {
            LOG.trace("Starting drive backward for " + millis + " milliseconds.");
        } else {
            LOG.trace("Starting drive forward for " + millis + " milliseconds.");
        }
        this.millis = millis;
        this.speed = speed;
    }
}