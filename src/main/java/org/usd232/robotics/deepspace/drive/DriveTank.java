package org.usd232.robotics.deepspace.drive;

import org.usd232.robotics.deepspace.commands.CommandBase;
import org.usd232.robotics.deepspace.log.Logger;

/**
 * Drives the robot in teleop based on left and right joystick inputs.
 * 
 * @author Zach Diebert, Alex Whipple, Brian Parks
 * @since Always
 * 
 */
public class DriveTank extends CommandBase {
    /**
     * The Logger.
     * 
     * @since 2018
     */
    private static final Logger LOG = new Logger();
    /**
     * The power given to the left motor
     * 
     * @since 2017
     */
    private double left;
    /**
     * The power given to the right motor
     * 
     * @since 2017
     */
    private double right;
    /**
     * The minimum power value
     * 
     * @since 2018
     * 
     */
    private double minValue = .3;
    /**
     * How far the input "dead zone" extends from 0
     * 
     * @since 2018
     */
    private double deadZone = 0.1;
    /**
     * Scaling factor to allow the "dead zone" to be a cube root function that
     * curves into.
     * 
     * @since 2018
     */
    private double deadZoneScale = minValue / Math.cbrt(deadZone);

    /**
     * {@inheritDoc}
     */
    @Override
    protected void execute() {
        LOG.catchAll(() -> {
            double joystick0 = oi.Joystick0.getY();
            double joystick1 = oi.Joystick1.getY();
            left = scaleInput(joystick0);
            right = scaleInput(joystick1);
            driveSubsystem.driveTank(-left, -right);
        });
    }

    /**
     * Scales the input of the joysticks.
     * 
     * @param input The input of the joysticks.
     * @return The scaled value that the robot should drive at.
     */
    private double scaleInput(double input) {
        double output = 0;
        double absoluteInput = Math.abs(input);
        if (absoluteInput < deadZone) {
            output = Math.cbrt(input) * deadZoneScale;
        } else {
            output = absoluteInput / input
                    * (((1 - minValue) / (1 - deadZone)) * (absoluteInput - deadZone) + minValue);
        }
        return output;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isFinished() {
        return false;
    }

    /**
     * Drives the robot in teleop based on left and right joystick inputs.
     * 
     * @since Always
     */
    public DriveTank() {
        requires(driveSubsystem);
    }
}
