package org.usd232.robotics.deepspace.commands;

import org.usd232.robotics.deepspace.OI;
import org.usd232.robotics.deepspace.RobotMap;
import org.usd232.robotics.deepspace.log.Logger;
import org.usd232.robotics.deepspace.subsystems.ClimbingSubsystem;
import org.usd232.robotics.deepspace.subsystems.DriveSubsystem;
import org.usd232.robotics.deepspace.subsystems.IntakeSubsystem;
import org.usd232.robotics.deepspace.subsystems.LiftSubsystem;
import edu.wpi.first.wpilibj.command.Command;

/**
 * This is a class that initializes the subsystems
 * 
 * @author Brian Parks, Zach Diebert
 * @since 2018
 */
public abstract class CommandBase extends Command implements RobotMap {
    /**
     * The Logger.
     * 
     * @since 2018
     */
    private static final Logger LOG = new Logger();
    /**
     * This is the OI.
     * 
     * @since Always
     */
    public static OI oi;
    /**
     * This is the drive subsystem.
     * 
     * @since Always
     */
    public static DriveSubsystem driveSubsystem;
    /**
     * This is the lift subsystem.
     * 
     * @since 2018
     */
    public static LiftSubsystem liftSubsystem;
    /**
     * This is the intake subsystem.
     * 
     * @since 2019
     */
    public static IntakeSubsystem intakeSubsystem;
    /**
     * This is the climbing subsystem.
     * 
     * @since 2019
     */
    public static ClimbingSubsystem climbingSubsystem;

    /**
     * Public initialize command for commands to use if needed.
     * 
     * @since 2018
     */
    public void initializePublic() {
        initialize();
    }

    /**
     * Public execute command for commands to use if needed.
     * 
     * @since 2018
     */
    public void executePublic() {
        execute();
    }

    /**
     * Public isFinished command for commands to use if needed.
     * 
     * @since 2018
     */
    public boolean isFinishedPublic() {
        return isFinished();
    }

    /**
     * Public end command for commands to use if needed.
     * 
     * @since 2018
     */
    public void endPublic() {
        end();
    }

    /**
     * This method initializes all of the subsystems.
     * 
     * @since 2018
     */
    public static void init() {
        driveSubsystem = new DriveSubsystem();
        liftSubsystem = new LiftSubsystem();
        intakeSubsystem = new IntakeSubsystem();
        climbingSubsystem = new ClimbingSubsystem();
        oi = new OI();
        LOG.info("Subsystems Have Been Initialized");
    }
}
